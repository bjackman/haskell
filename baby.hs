doubleMe x = x + x
doubleUs x y = doubleMe x + doubleMe y
doubleSmallNumber x = if x > 100
                        then x
                        else doubleMe x
f 0 = 1
f n = n * f (n - 1)

t :: (Show a) => [a] -> String
t [x] = "one " ++ show x
t [x, y] = "two " ++ show x ++ " " ++ show y
t (x:y:_) = "many"

