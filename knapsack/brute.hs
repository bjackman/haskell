--brute force solution to the knapsack problem
import Knapsack
main = runSolution knapsack

knapsack :: (Ord n, Num n) => Problem n -> Solution n
knapsack Problem { items = items, capacity = capacity }
  = chooseBest $ filter isValid $ combinations items
  where isValid s         = solCost s <= capacity
        chooseBest sols   = maximumBy compareValue sols
        compareValue a b  = compare (solScore a) (solScore b)

combinations :: [t] -> [[t]]
combinations []          = [[]]
combinations (head:tail) = tailSols ++ map (head:) tailSols
                            where tailSols = combinations tail

-- this is also in Data.List but I wanted to try and write my own version
maximumBy ::(a -> a -> Ordering) -> [a] -> a
maximumBy _ [only]      = only
maximumBy f (head:tail) = let tm = maximumBy f tail; ordering = f head tm
                          in if ordering == GT then head else tm
