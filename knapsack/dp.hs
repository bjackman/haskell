import System.Environment
import Data.Array
import Knapsack
import Debug.Trace

main = runSolution dp (fmap head getArgs)

i = Item 1 2 :: Item Int; j = Item 2 3 :: Item Int
p = Problem {items=[i,j], capacity=5}:: Problem Int

dp :: Problem Int -> Solution Int
dp p = getSol p (kt p)

--extract the solution from a DP table
getSol :: Problem Int -> Array (Int, Int) Int -> Solution Int
getSol Problem {items=[]} _ = []
getSol Problem {items=items, capacity=c} table =
  (getSol subProblem table)++this
    where n          = length items
          topIn      = if n > 0 then table!(n,c) > table!(n-1,c)
                                else table!(n, c) > 0
          i          = last items
          this       = if topIn then [i] else []
          subProblem = let c' = if topIn then c - cost i else c
                       in Problem {items=init items, capacity=c'}
                                             
--build the DP table
kt :: Problem Int -> Array (Int, Int) Int
kt Problem {items=items, capacity=capacity} = a
  where a = array ((0, 0), (length items, capacity)) l
        l = [((n, w), f n w) | n <- [0..length items], w <- [0..capacity]]
        f 0 _capacity = 0
        f n c = let newCost = cost (items!!(n-1))
                    newValue = value (items!!(n-1))
                    lowerCost = max (c - newCost) 0
                in if newCost <= c
                     then max (a!(n-1, c)) (a!(n-1, lowerCost) + newValue)
                     else a!(n-1,c)
