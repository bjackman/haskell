module Knapsack where 

import System.Environment
import System.IO
import System.CPUTime

runSolution :: Show a => (Problem Int -> a) -> IO String -> IO ()
runSolution solution fileNameIO = do
  fileName <- fileNameIO
  withFile fileName ReadMode (\h -> do
    text <- hGetContents h
    let problem = readProblem $ map words $ lines text in do
      t1 <- getCPUTime
      print $ solution problem
      t2 <- getCPUTime
      print (t2 - t1))

data Item a = Item { cost :: a, value :: a } deriving (Eq, Show)
type Solution a = [Item a]
data Problem a = Problem { items :: [Item a], capacity :: a } deriving (Show)

-- a solution's score is the sum of the values of the chosen items
solScore :: (Num n) => Solution n -> n
solScore items = sumMap items value

-- a solution's cost is the sum of the costs of the chosen items
solCost :: (Num n) => Solution n -> n
solCost items = sumMap items cost

-- are these in a library somewhere? anyway my own, because learning
sumMap :: (Num n) => [t] -> (t -> n) -> n
sumMap items f = sum $ map f items

--read a knapsack problems from a list of lines containing lists of words
readProblem :: [[String]] -> Problem Int
--ignore the first line, which is just a c convenience
readProblem (_:lines) = 
  let itemList = init lines; capString = last lines
  in Problem { items = map readItem itemList, capacity = read (head capString) }
  where readItem [_, value, cost] = 
          Item { value = read value, cost = read cost }
